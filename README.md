# Legend of the Five Rings (5th Edition) by [Edge Studio](https://edge-studio.net/)
![Banner Legend of the Five Rings](./l5rBan.jpg)

[![Buy Me a Coffee](https://img.shields.io/badge/Buy%20Me%20a-☕%20Coffee-red)](https://ko-fi.com/vlyan)
[![Forge Installs](https://img.shields.io/badge/dynamic/json?label=Forge%20Installs&query=package.installs&suffix=%25&url=https%3A%2F%2Fforge-vtt.com%2Fapi%2Fbazaar%2Fpackage%2Fl5r5e-world-tails&colorB=4aa94a)](https://forge-vtt.com/bazaar#package=l5r5e-world-tails)
[![Foundry Hub Endorsements](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e-world-tails%2Fshield%2Fendorsements)](https://www.foundryvtt-hub.com/package/l5r5e-world-tails/)
[![Foundry Hub Comments](https://img.shields.io/endpoint?logoColor=white&url=https%3A%2F%2Fwww.foundryvtt-hub.com%2Fwp-json%2Fhubapi%2Fv1%2Fpackage%2Fl5r5e-world-tails%2Fshield%2Fcomments)](https://www.foundryvtt-hub.com/package/l5r5e-world-tails/)

## The Knotted Tails / Les Queues Nouées
- Official adventure in english and french for Legend of the Five Rings 5th Edition.
- Aventure officielle en anglais et français pour la Légende des Cinq Anneaux 5eme Edition.


# English
This is a script for [Foundry Virtual Tabletop (FVTT)](https://foundryvtt.com/).
This version is authorized by Edge Studio, all texts, images and copyrights are the property of their respective owners.

## Install with the manifest
1. Copy this link and use it in Foundrys world manager to install the world.
    > Manifest: https://gitlab.com/teaml5r/l5r5e-world-tails/-/raw/master/world/world.json

## Current L5R team (alphabetical order)
- Carter (compendiums, adventure adaptation)
- Vlyan (development)

## Contribute
You are free to contribute and propose corrections, modifications after fork.

See the [Wiki page - System helping (Contribute)](https://gitlab.com/teaml5r/l5r5e/-/wikis/dev/system-helping.md) for detail.


# French
Il s'agit d'un scénario de jeu pour [Foundry Virtual Tabletop (FVTT)](https://foundryvtt.com/).
Cette version est autorisée par Edge Studio, tous les textes, images et droits d'auteur reviennent à leur propriétaires respectifs.

## Installer avec le manifeste
Copier ce lien et chargez-le dans le menu Mondes de jeu de Foundry
    > Manifest: https://gitlab.com/teaml5r/l5r5e-world-tails/-/raw/master/world/world.json

## Nous rejoindre
1. Vous pouvez retrouver toutes les mises à jour en français pour FoundryVTT sur le discord officiel francophone
2. Lien vers [Discord Francophone](https://discord.gg/pPSDNJk)

## L'équipe L5R actuelle (par ordre alphabétique)
- Carter (compendiums, adaptation de scénario)
- Vlyan (développement)

## Remerciements, vielen danke & Many thanks to :
1. José Ladislao Lainez Ortega Aka L4D15, pour sa première version
2. Sasmira et LeRatierBretonnien pour leur travail sur la [communauté Francophone de Foundry](https://discord.gg/pPSDNJk)
3. Flex pour ses conseils

## Contribuer
Vous êtes libre de contribuer et proposer après fork des corrections, modifications.

Voir la [page du Wiki - System helping (Contribute)](https://gitlab.com/teaml5r/l5r5e/-/wikis/dev/system-helping.md) pour le détail (en anglais).


# Update/Mise à jour
Date format : day/month/year
- 29/12/2022 - v1.0.2 : Foundry v10 update.
- 21/07/2021 - v1.0.1 : Added token images for Actors
- 18/07/2021 - v1.0.0 : Initial release.
